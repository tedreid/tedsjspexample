package com.remedy;

import java.io.File;

/**
 * Created by treid on 1/27/16.
 */
public class Main {
    public static void main(String[] args) {
        String procName = "WEB-INF/lib/hello";
        String [] params =  new String[]{"one", "two", "three", "four", "five"};
        File file = new File(procName);
        if (file.exists()){
            System.err.println("It's there!");
        }

        GetResponseFromC responseFromC = new GetResponseFromC(procName, "", params);
        String output = responseFromC.execute();
        System.out.println(output);
    }
}
