package com.remedy;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.json.*;


/**
 * Created by treid on 1/27/16.
 */
public class GetResponseFromC {
    public static String getHelloFromClass(){
        return "Hello from the class file!";
    }

    private String executableName;
    private String workingDir;
    private String[] params;
    public GetResponseFromC(String executableName, String workingDir, String [] params){
        this.executableName = executableName;
        this.params = params;
        this.workingDir = workingDir;
    }

    public String execute() {


        String exitingOutput = "";
        List<String> parameters = new ArrayList<String>();
        parameters.add(executableName);
        for(String param : params) {
           parameters.add(param);
        }
        try {
        Process myHelloProcess = new ProcessBuilder().command(parameters).start();
        InputStream myHelloOut = myHelloProcess.getInputStream();
        BufferedReader helloOutput = new BufferedReader(new InputStreamReader(myHelloOut));
        String line;

        while (myHelloProcess.isAlive()) {
            Thread.sleep(500);
        }
        while ((line = helloOutput.readLine()) != null) {

            exitingOutput += line + "\n";
        }
        System.out.println("The process exit code was " + myHelloProcess.exitValue());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return exitingOutput;
    }

    public List<String> executeList(){
        List<String> exitingOutput = new ArrayList<String>();
        List<String> parameters = new ArrayList<String>();
        parameters.add(executableName);
        for(String param : params) {
            parameters.add(param);
        }
        try {

            ProcessBuilder myProc = new ProcessBuilder().command(parameters);

            myProc.directory(new File(workingDir));
            Process myHelloProcess = myProc.start();


            InputStream myHelloOut = myHelloProcess.getInputStream();
            BufferedReader helloOutput = new BufferedReader(new InputStreamReader(myHelloOut));
            String line;

            while (myHelloProcess.isAlive()) {
                Thread.sleep(500);
            }
            while ((line = helloOutput.readLine()) != null) {

                exitingOutput.add(line + "\n");
            }
//            System.out.println("The process exit code was " + myHelloProcess.exitValue());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return exitingOutput;
    }

    public String executeJSON() {

        List<String> exitingOutput = new ArrayList<String>();
        List<String> parameters = new ArrayList<String>();
        parameters.add(executableName);
        for(String param : params) {
            parameters.add(param);
        }
        try {
            Process myHelloProcess = new ProcessBuilder().command(parameters).start();
            InputStream myHelloOut = myHelloProcess.getInputStream();
            BufferedReader helloOutput = new BufferedReader(new InputStreamReader(myHelloOut));
            String line;

            while (myHelloProcess.isAlive()) {
                Thread.sleep(500);
            }
            int lineNumber = 1;
            exitingOutput.add("{");
            while ((line = helloOutput.readLine()) != null) {

                exitingOutput.add("\"" + lineNumber + "\"" + ":" + "\"" + line + "\"" + ",");
                lineNumber++;
            }
            System.out.println("The process exit code was " + myHelloProcess.exitValue());
            exitingOutput.add("}");

        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject obj = new JSONObject(exitingOutput.toString());
        String jsonString = obj.toString();
        return jsonString;
    }
}
