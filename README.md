# README #

### What is this repository for? ###

* This is just an example I set up to show a client how to call a native C application from Java, along with passing parameters to it from a POST, and displaying the response/output.

### How do I get set up? ###
* This project is pretty simple to get up and running using IntelliJ and Tomcat.
* Deploy the exploded war to tomcat. POST parameters without names to /secondIndex.jsp It will echo any parameters back out to you in the response.

### Contribution guidelines ###

* No contributions needed at this time. Personal Project.

### Who do I talk to? ###

* tedreid@gmail.com