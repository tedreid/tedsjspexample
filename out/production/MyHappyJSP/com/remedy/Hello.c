#include <stdio.h>

int main(int argc, char* argv[] ) {
    if (argc < 2) {
    printf("No arguments were supplied\n");
    return 0;
    }
    for (int i = 0; i < argc; i++){
        printf("The %2D arg is: %s\n", i, argv[i]);
    }
}