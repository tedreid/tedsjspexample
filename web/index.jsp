<%--
  Created by IntelliJ IDEA.
  User: treid
  Date: 1/27/16
  Time: 11:41 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.remedy.GetResponseFromC" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.File" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  This is the JSP.<br>
  <% out.println("Hello from Java!");%>
  <br>
  <%
    String procName = "hello";
    String workingDir =  getServletConfig().getServletContext().getRealPath("WEB-INF/classes/com/remedy/");
    String fullProcName = getServletConfig().getServletContext().getRealPath("WEB-INF/classes/com/remedy/" + procName);
    File tryme = new File(getServletConfig().getServletContext().getRealPath("WEB-INF/classes/com/remedy/hello"));
    if (tryme.exists()){
      System.err.println("********* It found the files!");
    }

    Enumeration<String> paramNames = request.getParameterNames();
    List<String> parametersList = new ArrayList<String>();
    List<String> parameters = new ArrayList<String>();
    while (paramNames.hasMoreElements()){
      parameters.add(request.getParameter(paramNames.nextElement()));
    }

    String [] params = parameters.toArray(new String[parameters.size()]);
//    String [] params =  new String[]{"one", "two", "three", "four", "five"};
    String foo = GetResponseFromC.getHelloFromClass();
    GetResponseFromC responseFromC = new GetResponseFromC(fullProcName, workingDir, params);

    out.println(responseFromC.execute());
  %>
  </body>
</html>
