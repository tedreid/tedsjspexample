<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.remedy.GetResponseFromC" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="org.json.JSONTokener" %>
<%@ page import="java.io.File" %>
<%
    String procName = "hello";
    String workingDir =  getServletConfig().getServletContext().getRealPath("WEB-INF/classes/com/remedy/");
    String fullProcName = getServletConfig().getServletContext().getRealPath("WEB-INF/classes/com/remedy/" + procName);
    File tryme = new File(getServletConfig().getServletContext().getRealPath("WEB-INF/classes/com/remedy/hello"));
    if (tryme.exists()){
      System.err.println("********* It found the files!");
    }

    List<String> parameters = new ArrayList<String>();
    BufferedReader reader = request.getReader();
    String line = "";
    while ((line = reader.readLine()) != null){
      parameters.add(URLDecoder.decode(line));
    }

    String [] params = parameters.toArray(new String[parameters.size()]);
//    String [] params =  new String[]{"one", "two", "three", "four", "five"};

    GetResponseFromC responseFromC = new GetResponseFromC(fullProcName, workingDir, params);

    out.println(responseFromC.execute());
  %>

